/*
 * gauss.cpp
 *
 *  Created on: Apr 5, 2017
 *      Author: leandro
 */

#include "gauss.h"



gauss::gauss() {
	// TODO Auto-generated constructor stub
	n=20;

}
gauss::gauss(int N) {
	n=N;
	t=gsl_integration_glfixed_table_alloc (n);
	
	c2=-0.25;
	x1=new double [n];
	x2=new double [n];
	w1=new double [n];
	w2=new double [n];
	atanh_x=new double [n];
	cos_2x=new double [n];
	sin_v=new double [n];
	cos_v=new double [n];
	
    for(int i=0;i<n;i++){
    	 gsl_integration_glfixed_point (0,1, i, &x1[i], &w1[i], t);
    	 gsl_integration_glfixed_point (0,pi, i, &x2[i], &w2[i], t);
		 
		 atanh_x[i]=atanh(x1[i]);
		 double v=x2[i]+c2*sin(2*x2[i]);
    	 cos_2x[i]=cos(2*x2[i]);
		 sin_v[i]=sin(v);
		 cos_v[i]=cos(v);
     }
     
    xc_func_init(&correlacion, XC_LDA_C_VWN, XC_UNPOLARIZED);
	xc_func_init(&exchange, XC_LDA_X, XC_UNPOLARIZED); 
     
}
gauss::~gauss() {
	delete [] x1;
	delete [] x2;
	delete [] w1;
	delete [] w2;
	delete []	atanh_x;
	delete [] cos_2x;
	delete [] sin_v;
	delete [] cos_v;
	gsl_integration_glfixed_table_free(t);
	xc_func_end(&correlacion);
	xc_func_end(&exchange);
}

void gauss::generate_grid(double a,double c1,double *rr,double *t_1,double *t_2,double *sin_1,double *sin_2,double *cos_1,double *cos_2){
    
	
	for(int j=0;j<n;j++){
		double u=c1*atanh_x[j];
		double	 cosh_u=cosh(u);
		double sinh_u=sinh(u);		
    	for (int k=0;k<n/2;k++){
    		 
    		
 

             
                
            
    		 double r1=a*(cosh_u+cos_v[k]);
             double r2=a*(cosh_u-cos_v[k]);
             double r=a*sinh_u*sin_v[k];
             double z=a*cosh_u*cos_v[k];
             sin_1[j*n+k]=(r/r1);
             sin_2[j*n+k]=(r/r2);
             cos_1[j*n+k]=(z+a)/r1;
             cos_2[j*n+k]=(z-a)/r2;

			 sin_2[j*n+n-k-1]=sin_1[j*n+k];
    		 sin_1[j*n+n-k-1]=sin_2[j*n+k];
			 cos_2[j*n+n-k-1]=-cos_1[j*n+k];
			 cos_1[j*n+n-k-1]=-cos_2[j*n+k];

			 rr[j*n+k]=r*(c1/(1-x1[j]*x1[j]))*(1+2*c2*cos_2x[k]);
			
			 rr[j*n+n-k-1]=rr[j*n+k];

             t_1[j*n+k]=-10;
             t_2[j*n+k]=-10;
			
			double rt[2]={r1,r2};
			double tt[2];

			#pragma omp simd
			for (int i=0;i<2;i++){
				tt[i]=log(rt[i]);
			}
             
			t_1[j*n+k]=tt[0];
			t_2[j*n+k]=tt[1];

			t_2[j*n+n-k-1]=t_1[j*n+k];
			t_1[j*n+n-k-1]=t_2[j*n+k];			


		}


	}



}


void gauss::integrate_potential(MySpline  *veffa,vector<MySpline*> &veffb,vector<MySpline*> &vconfb,MySpline *veff0b,vector<MySpline*> &A,vector<MySpline*> &B,double*s,double*y,int z1,int z2,double r1min,double r2min,double rmax1,double rmax2,double distance){
    
    double a=distance;
    double c1=(2./log(3.))*acosh(1.+0.4/a);

    double log_Z1=log(z1);
    double log_Z2=log(z2);

	double *t_1= new double [n*n];
	double *t_2= new double [n*n];
	double *sin_1=new double [n*n];
	double *sin_2=new double [n*n];
	double *cos_1=new double [n*n];
	double *cos_2=new double [n*n];
	double *rr=new double [n*n];

	generate_grid(a, c1,rr, t_1, t_2,sin_1,sin_2,cos_1,cos_2);

	for (int j=0;j<n;j++){
		for(int k=0;k<n;k++){

			double veff1;
			double veff2[3];
			double veff02;
			double DVb[3];
			double vconf2[3];
			double R1[3];
			double R2[3];
    
            double rho[0];
            double vx[0];
            double vc[0];
            double ec[0];
            double ex[0];
            double rho_a=0;
            double rho_b=0;
			double vxc_a=0;
            double vxc_b=0;

			int index=j*n+k;

			double t1=t_1[index]+log_Z1;
			double t2=t_2[index]+log_Z2;

			double sin1,sin2,cos1,cos2;

			sin1=sin_1[index];
			sin2=sin_2[index];
			cos1=cos_1[index];
			cos2=cos_2[index];

			
			double dV=rr[index];//*(c1/(1-x1[j]*x1[j]))*(1+2*c2*cos_2x[k]); 
             
             

				//t1=log(Z1*r1);
				
				veff1=veffa->inter(t1);
				
			
			 
			 
             

				veff02=veff0b->inter(t2);
				for(int i=0;i<3;i++){
					vconf2[i]=vconfb[i]->inter(t2);
					veff2[i]=veffb[i]->inter(t2);
					}
				
		     
		     for(int i=0;i<3;i++){
				 DVb[i]=veff02-veff2[i]-vconf2[i];
				 }
             

    				 for(int i=0;i<3;i++){
    					 R1[i]=A[i]->inter(t1);
						 R2[i]=B[i]->inter(t2);
						 
    				 }

    			
    		 double si[10];

			double w=w1[j]*w2[k]*dV;

			 si[0]=w*R1[0]*R2[0]*ss(sin1,sin2,cos1,cos2); //ss
             si[1]=w*R1[0]*R2[1]*sp(sin1,sin2,cos1,cos2); //sp
             si[2]=w*R1[0]*R2[2]*sd(sin1,sin2,cos1,cos2); //sd
             si[3]=w*R1[1]*R2[1]*pp_pi(sin1,sin2,cos1,cos2); //pp_pi
             si[4]=w*R1[1]*R2[1]*pp_sig(sin1,sin2,cos1,cos2); //pp_sig
             si[5]=w*R1[1]*R2[2]*pd_pi(sin1,sin2,cos1,cos2); //pd_pi
             si[6]=w*R1[1]*R2[2]*pd_sig(sin1,sin2,cos1,cos2); //pd_sig
             si[7]=w*R1[2]*R2[2]*dd_del(sin1,sin2,cos1,cos2); //dd_del
             si[8]=w*R1[2]*R2[2]*dd_pi(sin1,sin2,cos1,cos2); //dd_pi
             si[9]=w*R1[2]*R2[2]*dd_sig(sin1,sin2,cos1,cos2); //dd_sig
           
    		 for(int l=0;l<10;l++){
				s[l]+=si[l]; 
				}
    		 y[0]+=si[0]*(veff1+DVb[0]); //ver que pasa con el confinamiento
    		 y[1]+=si[1]*(veff1+DVb[1]);
    		 y[2]+=si[2]*(veff1+DVb[2]);
    		 y[3]+=si[3]*(veff1+DVb[1]);
    		 y[4]+=si[4]*(veff1+DVb[1]);
    		 y[5]+=si[5]*(veff1+DVb[2]);
    		 y[6]+=si[6]*(veff1+DVb[2]);
    		 y[7]+=si[7]*(veff1+DVb[2]);
    		 y[8]+=si[8]*(veff1+DVb[2]);	
    		 y[9]+=si[9]*(veff1+DVb[2]);
    		}

    	}

	delete [] t_1;
	delete [] t_2;
	delete [] sin_1;
	delete [] sin_2;
	delete [] cos_1;
	delete [] cos_2;
	delete []  rr;
}






void gauss::integrate_density(MySpline *veffa,vector<MySpline*> &veffb,vector<MySpline*> &vconfb,MySpline *veff0b,vector<MySpline*> &A,vector<MySpline*> &B,MySpline *vxca,MySpline *vxcb,MySpline *dens_a,MySpline *dens_b,double*s,double*y,int z1,int z2,double r1min,double r2min,double rmax1,double rmax2,double distance){
    
    double a=distance;
    double c1=(2./log(3.))*acosh(1.+0.4/a);

    double log_Z1=log(z1);
    double log_Z2=log(z2);
 
     double rmin1=log(z1*r1min);
     double rmin2=log(z2*r2min);
     double r1max=log(z1*rmax1);
     double r2max=log(z2*rmax2);

	double *t_1= new double [n*n];
	double *t_2= new double [n*n];
	double *sin_1=new double [n*n];
	double *sin_2=new double [n*n];
	double *cos_1=new double [n*n];
	double *cos_2=new double [n*n];
	double *rr=new double [n*n];

	generate_grid(a, c1,rr, t_1, t_2,sin_1,sin_2,cos_1,cos_2);

    
	for (int j=0;j<n;j++){
		for(int k=0;k<n;k++){

			double veff1;
			double veff2[3];
			double veff02;
			double DVb[3];
			double vconf2[3];
			double R1[3];
			double R2[3];
    
            double rho[0];
            double vx[0];
            double vc[0];
            double ec[0];
            double ex[0];
            double rho_a=0;
            double rho_b=0;
			double vxc_a=0;
            double vxc_b=0;

			double t1=t_1[j*n+k]+log_Z1;
			double t2=t_2[j*n+k]+log_Z2;

			double sin1,sin2,cos1,cos2;

			sin1=sin_1[j*n+k];
			sin2=sin_2[j*n+k];
			cos1=cos_1[j*n+k];
			cos2=cos_2[j*n+k];

			
			double dV=rr[j*n+k];//*(c1/(1-x1[j]*x1[j]))*(1+2*c2*cos_2x[k]);                //r=a*sinh(u)*sin(v);

			if(t1>r1max  or t1<rmin1 ) {
				
					veff1=0;
				
				}
			else{
				//t1=log(Z1*r1);
				veff1=veffa->inter(t1);
					
					}
				
			 
			 
             
			if(t2>r2max or t2<rmin2){
				 veff02=0;
				 for(int i=0;i<3;i++){
					veff2[i]=0;
					vconf2[i]=0;
					}
				 }
					
    		 else {
				//t2=log(Z2*r2);
				veff02=veff0b->inter(t2);
				for(int i=0;i<3;i++){
					vconf2[i]=vconfb[i]->inter(t2);
					veff2[i]=veffb[i]->inter(t2);
					
					}
				}
		     
		     for(int i=0;i<3;i++){
				 DVb[i]=veff02-veff2[i]-vconf2[i];
				 }
             
    		 if(t1>r1max or t2>r2max){
				 for(int i=0;i<3;i++){
    				 R1[i]=0;
    				 R2[i]=0;
					}
				 }
    		 else{
    				 for(int i=0;i<3;i++){
    					 R1[i]=A[i]->inter(t1);
						 R2[i]=B[i]->inter(t2);

    				 }

    			 }
    			 
			if(t1>r1max or t2>r2max){
				vxc_a=0;
				vxc_b=0;
				rho_a=0;
				rho_b=0;	

				}
			else{
				vxc_a=vxca->inter(t1);
				vxc_b=vxcb->inter(t2);
				
				rho_a=dens_a->inter(t1);
				rho_b=dens_b->inter(t2);
				}	    
						 
    		*rho=rho_a+rho_b;	 
    		//cout<<*rho<<endl; 	 
    			//********correlacion*********
	         xc_lda_exc_vxc(&correlacion, 1, rho, ec,vc);


              //********intercambio*************


             xc_lda_exc_vxc(&exchange, 1, rho, ex,vx);	 
    		 
    		 double DVxc=*vc+*vx-vxc_a-vxc_b;
    		 
    		 double si[10];
			 double w=w1[j]*w2[k]*dV;

             si[0]=w*R1[0]*R2[0]*ss(sin1,sin2,cos1,cos2); //ss
             si[1]=w*R1[0]*R2[1]*sp(sin1,sin2,cos1,cos2); //sp
             si[2]=w*R1[0]*R2[2]*sd(sin1,sin2,cos1,cos2); //sd
             si[3]=w*R1[1]*R2[1]*pp_pi(sin1,sin2,cos1,cos2); //pp_pi
             si[4]=w*R1[1]*R2[1]*pp_sig(sin1,sin2,cos1,cos2); //pp_sig
             si[5]=w*R1[1]*R2[2]*pd_pi(sin1,sin2,cos1,cos2); //pd_pi
             si[6]=w*R1[1]*R2[2]*pd_sig(sin1,sin2,cos1,cos2); //pd_sig
             si[7]=w*R1[2]*R2[2]*dd_del(sin1,sin2,cos1,cos2); //dd_del
             si[8]=w*R1[2]*R2[2]*dd_pi(sin1,sin2,cos1,cos2); //dd_pi
             si[9]=w*R1[2]*R2[2]*dd_sig(sin1,sin2,cos1,cos2); //dd_sig
             
    		 for(int l=0;l<10;l++){
				s[l]+=si[l]; 
				}
    		 y[0]+=si[0]*(veff1+DVb[0]+DVxc); //ver que pasa con el confinamiento
    		 y[1]+=si[1]*(veff1+DVb[1]+DVxc);
    		 y[2]+=si[2]*(veff1+DVb[2]+DVxc);
    		 y[3]+=si[3]*(veff1+DVb[1]+DVxc);
    		 y[4]+=si[4]*(veff1+DVb[1]+DVxc);
    		 y[5]+=si[5]*(veff1+DVb[2]+DVxc);
    		 y[6]+=si[6]*(veff1+DVb[2]+DVxc);
    		 y[7]+=si[7]*(veff1+DVb[2]+DVxc);
    		 y[8]+=si[8]*(veff1+DVb[2]+DVxc);	
    		 y[9]+=si[9]*(veff1+DVb[2]+DVxc);
    		 

		     
    	 }
     }

	delete [] t_1;
	delete [] t_2;
	delete [] sin_1;
	delete [] sin_2;
	delete [] cos_1;
	delete [] cos_2;
	delete []  rr;



		
	}







