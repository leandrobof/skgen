/*
 * scf.h
 *
 *  Created on: 24/05/2017
 *      Author: leandro
 */

#ifndef SCF_H_
#define SCF_H_
#include "Orbital.h"
#include "hartree.h"
#include "xc_potential.h"
#include <map>
#include <sstream>

enum ocupation{Average=0,Energy=1};



void dens_inicial(double *r,double *rho,int N,int Z);


/**
 * @brief Actualiza la densidad y gradiente a partir de la funciones radiales de los nuevos orbitales y la antigua densidad.
 * $$\rho_{new}=(1-\alpha)\rho+\alpha\rho_{old}$$
 * @param rho =  densidad.
 * @param grad = gradiente.
 * @param Atom = Orbitales atomicos.
 * @param alfa = parametro de damping.
 * @param N = tamaño grilla de rho y grad.
 */

void new_rho(double *rho, vector<Orbital> &Atom,double alfa,int N);

/**Esta clase realiza calculo scf para un atomo.
*
*Realiza calculo scf para un determinado atomo, y parametros de confinamiento.
*Ademas devuelve cantidades que son necesarias para el calculo de integrales Slater-Koster.
*/
class Scf{
    private:
        int Z;
        int N;
        double h;
        double max;
        double min;
        double *t;
        double *r;
        double *veff_new;
        double *veff_old;
        double *vn;
        double *vconf_new;
        double *vconf_old;
        double *rho;
        double *grad;
        double *vxc_final;
        double *rho_final;
        double alfa;
        double W;
        double a;
        double r0;
        bool Relativistic;
        bool gga;
        bool restart;
        bool save;
        ocupation ocup_type;
        string SK[3];
        string prefix;
        Xc *vxc;
        std::map<char,int> x;
        std::map<string,int> index;
        vector <Orbital*> Atom;
        double *rad[3];
        double *vconf_basis[3];
        double *veff_basis[3];
        double *veff_dens;
        double *vconf_dens;
        vector<double> econf={0,0,0};
        string restart_file;

    public:
        /**
         * Constructor
         * Construye scf a valores por defecto.
         */
        Scf();
        ~Scf();

        /**Inicializa parametros a partir de archivo.
         * 
         * Lee archivo y obtiene Z, la configuracion electronica, tipo de calculo:Relativista no relativista,
         * tipo de Funcional.Con esto crea los orbitales.
         * @param archivo Archivo con informacion sobre el atomo
         */
        void initialize(string archivo);




        /**Corre un calculo autoconsistente.
         *
         * Realiza calculo de orbitales de forma autoconsistente para un determinado potencial de confinamiento de Woods-Saxon.
         *\f$ V_{conf}(r)=W/(1+e^{-a r/r_0}) \f$
        *
        * @param W    parametro de confinamiento.
        * @param a    parametro de confinamiento.
        * @param r0   parametro de confinamiento.
        * @param alf  damping.
        */
        void run(double W,double a,double r0,double alf);


        /**
         *@return Z. Numero atomico.  
        */
        int z(){return Z;};


        /**
         *@return N. Tamaño de la grilla.
        */
        int Nt(){return N;}



        /**
         * 
         * @param e  vector donde se guardaran autovalores.
         * @param nocup vector donde se guardaran las ocupaciones.
         * @note En el caso Relativista devuelve un valor promedio de acuerdo con (FORMULA) ...
         */
        void energy(vector<double> &e,vector<double> &nocup);




    
        void energy(int i);


        /**
         *@return t :puntero a grilla t.
        */
        double* tgrid(){return t;}


        /**@return r : puntero a grilla  r (radial).
         *
         */
        double* rgrid(){return r;}



        /**
         *
         * @return puntero a Vconfinamiento.
         */
        double* Vconf_basis(int i){return vconf_basis[i];};


        double* Vconf_dens(){return vconf_dens;};

        double* Veff_basis(int i){return veff_basis[i];  };

        double* Veff_dens(){return veff_dens;};



        /**
         * Lee archivo con autovalores y veff. 
         */
        void readpot();

        /**
         * Guarda archivo con autovalores  e y veff.
         */
        void writepot();
        void write_atom();

        void save_pot();
        void save_pot(int);
        void save_dens();
        
        void save_basis();
        void save_basis(int);

        double* radial(int);


        double get_econf(int i){return econf[i];}
        double* get_rho_final(){return rho_final;}
        double* get_vxc_final(){return vxc_final;}
    };
#endif /* SCF_H_ */
