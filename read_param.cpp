#include "read_param.h"


using namespace rapidjson;

void read_table_opt(char* archivo,SKtable& sk){
    ifstream entrada(archivo);
	Document input;

    IStreamWrapper isw(entrada);	
	input.ParseStream(isw);

	if (input.HasParseError()){
		throw "ERROR Parsing JSON file: "+string(archivo);
	}

    //Parseo de opciones
	Value Options=input["TableOptions"].GetObject();
	
	if (Options.HasMember("ngrid")){
		sk.set_grid(Options["ngrid"].GetInt());
	}

	if (Options.HasMember("rmax")){
		sk.set_rmax(Options["rmax"].GetDouble());
	}
	
	if (Options.HasMember("rmin")){
		sk.set_rmin(Options["rmin"].GetDouble());
	}
    
	if (Options.HasMember("step")){
		sk.set_step(Options["step"].GetDouble());
	}
		
	if (Options.HasMember("superposition_type")){
		sk.set_superposition(Options["superposition_type"].GetString());
	}
	entrada.close();


}




atom read_table_atom(char* archivo)  {
    //Parseo de atomos
    ifstream entrada(archivo);
	Document input;

    IStreamWrapper isw(entrada);	
	input.ParseStream(isw);

	if (input.HasParseError()){
		throw "ERROR Parsing JSON file: "+string(archivo);
	}	
	
    //Parseo de opciones


    atom atparam;
	
    Value Atoms = input["Atoms"].GetArray();
	
    	
	for (int i=0;i<Atoms.Size();i++){
		Value Atom=Atoms[i].GetObject();
		atparam.symbol.push_back(Atom["element"].GetString());				
		vector<double> bas={0,0,0};
		vector<double> dens={0,0,0};
		vector<double> Uparse={0.,0.,0.};
		vector<double> eparse={2.1,2.1,2.1};
		vector<double> s={0,0,0};
		vector<double> p={0,0,0};
		vector<double> d={0,0,0};
		vector<bool> orbital={false,false,false};
		bool has_dens=false;
		
		if(Atom.HasMember("basis")){
			Value arr=Atom["basis"].GetArray();
			bas[0]=arr[0].GetDouble();
			bas[1]=arr[1].GetDouble();
			bas[2]=arr[2].GetDouble();
		}
		atparam.param_basis.push_back(bas);		
		
		if(Atom.HasMember("dens")){
			Value arr=Atom["dens"].GetArray();
			dens[0]=arr[0].GetDouble();
			dens[1]=arr[1].GetDouble();
			dens[2]=arr[2].GetDouble();
			has_dens=true;
		}
		else{dens=bas;} 
		atparam.param_dens.push_back(dens);
				
		if(Atom.HasMember("s")){
			Value arr=Atom["s"].GetArray();
			s[0]=arr[0].GetDouble();
			s[1]=arr[1].GetDouble();
			s[2]=arr[2].GetDouble();
			orbital[0]=true;
		}
	    atparam.param_s.push_back(s);
	    
		if(Atom.HasMember("p")){
			Value arr=Atom["p"].GetArray();
			p[0]=arr[0].GetDouble();
			p[1]=arr[1].GetDouble();
			p[2]=arr[2].GetDouble();
			orbital[1]=true;
		}
		atparam.param_p.push_back(p);
				
		if(Atom.HasMember("d")){
			Value arr=Atom["d"].GetArray();
			d[0]=arr[0].GetDouble();
			d[1]=arr[1].GetDouble();
			d[2]=arr[2].GetDouble();
			orbital[2]=true;
		}
        atparam.param_d.push_back(d);		
		
		if(Atom.HasMember("Ud")){
			 Uparse[2]=Atom["Ud"].GetDouble();
		}
		
		if(Atom.HasMember("Up")){
			 Uparse[1]=Atom["Up"].GetDouble();
		}
				
		if(Atom.HasMember("Us")){
			 Uparse[0]=Atom["Us"].GetDouble();
		}
				
		if(Atom.HasMember("ed")){
			 eparse[2]=Atom["ed"].GetDouble();
		}
				
		if(Atom.HasMember("ep")){
			 eparse[1]=Atom["ep"].GetDouble();
		}
				
		if(Atom.HasMember("es")){
			 eparse[0]=Atom["es"].GetDouble();
		}
		
		
		atparam.U.push_back(Uparse);
		atparam.e.push_back(eparse);
		atparam.has_param_orbital.push_back(orbital);
		atparam.has_param_dens.push_back(has_dens);
	}
	 

	entrada.close();
    return atparam;
    };