/*
 * SKtable.h
 *
 *  Created on: Feb 27, 2018
 *      Author: leandro
 */
#include "globals.h"
#include "Orbital.h"
#include "gauss.h"
#include "scf.h"

#ifndef SKTABLE_H_
#define SKTABLE_H_

class SKtable {
private:
	double step;
	double rmin;
	double rmax;
	int ngrid;
	
	bool homonuclear;
        vector<MySpline*> A_spline;
        vector<MySpline*> B_spline;
        MySpline *Veffa;
        MySpline *Veff0b;
        vector<MySpline*> Veffb;
        vector<MySpline*> Vconf;
        int Z1,Z2;
        vector<double> eb;
        double r1min;
	double r2min;
	double r1max;
	double r2max;

        
        MySpline *Vxca;
	MySpline *Vxcb;
	MySpline *dens_a;
	MySpline *dens_b; 
    enum sup { potential,density};
    sup superposition; 
    vector<double> H_onsite={0.,0.,0.};
    bool calc_onsite;
public:
	SKtable();
        SKtable(Scf & , Scf &);
	virtual ~SKtable();
	void create( bool);
        void set_grid(int );
        void set_rmax(double);
        void set_rmin(double);
        void set_step(double);
        void set_superposition(string);
        void run(vector<double> &e,vector<double> &U,vector<double> &ocupation,string archivo);
        double  onsite(double *Veffb, double *Vconf,double *Veff0b,double *R,double ebconf,int N,double *t);
};

#endif /* SKTABLE_H_ */
