/*
 * scf.cpp
 *
 *  Created on: 24/05/2017
 *      Author: leandro
 */
#include "scf.h"
using namespace rapidjson;
typedef std::numeric_limits< double > dbl;

void dens_inicial(double *r,double *rho,int N,int Z){
	double x;
	double a=0.7280642371;
	double b=-0.5430794693;
	double gamma=0.3612163121;
	double Zeff;
	double v;
	for(int i=0;i<N;i++){
		x=r[i]*pow((128*Z/9*pi*pi),1/3.);
		Zeff=Z*(1+a*sqrt(x)+b*x*exp(-gamma*sqrt(x)))*(1+a*sqrt(x)+b*x*exp(-gamma*sqrt(x)))*exp(-2*a*sqrt(x));
        v=Zeff/r[i];
        rho[i]=-1/(3*pi*pi)*pow(-2*v,3/2.);
	}
}


void new_rho(double *rho, double *grad,vector<Orbital*> &Atom,double alfa,int N){
	double nrho;
	double ngrad;
	for(int i=0;i<N;i++){
		nrho=0;
		ngrad=0;
		for (int j=0;j<Atom.size();j++){
	         nrho=nrho+(Atom[j])->operator[](i);
             ngrad=ngrad+(Atom[j]->grd(i));
	    }
		rho[i]=(1-alfa)*rho[i]+alfa*nrho;
		grad[i]=(1-alfa)*grad[i]+alfa*ngrad;

	}
};
Scf::Scf(){
	Z=0;
	h=0.008;
	max=50;
	min=-8;
	alfa=0.3;
	W=0.;
	a=1.;
	r0=1.;
	Relativistic=false;
	gga=false;
	restart=false;
	save=false;
	ocup_type=Average;
	x['s']=0;
	x['p']=1;
	x['d']=2;
	x['f']=3;

}

void Scf::initialize(string archivo){
	ifstream atomo(archivo);
    vector<string> config;    
    prefix=archivo;

	IStreamWrapper isw(atomo);
	Document input;
	input.ParseStream(isw);

	if (input.HasParseError()){
		throw "ERROR in JSON file: "+archivo;
	}

	if(input.HasMember("Relativistic")){
		Relativistic = input["Relativistic"].GetBool();
	}

	if(input.HasMember("GGA")){
		gga = input["GGA"].GetBool();
	}
	
	if(input.HasMember("restart file")){
		restart=true;
		restart_file= input["restart file"].GetString();
	}
	
	if(input.HasMember("save")){
		save = input["save"].GetBool();
	}
	
	if(input.HasMember("z")){
		Z=input["z"].GetInt();
	}else{
		throw "ERROR: z argument not present";
	}
	
	if (input.HasMember("sk_orb")){
		Value sk_orbital=input["sk_orb"].GetArray();
		for (int j = 0; j < 3; j++){
			SK[j] = sk_orbital[j].GetString();
		};
	}
	else{
		throw "ERROR: sk_orb argument not present";
	}

	if (input.HasMember("config")){
		istringstream iss(input["config"].GetString());
		string s;
		while (iss >> s){
			config.push_back(s);
		}
	}
	else{
		throw "ERROR: config argument not present";
	}





	atomo.close();

	N=(log(Z*max)-min)/h;
	t=new double [N];
	r=new double [N];
	veff_new=new double [N];
//	veff_old=new double [N];
	vn=new double [N];
	vconf_new=new double [N];
//	vconf_old=new double [N];
	vconf_dens=new double [N];
	veff_dens=new double [N];
	rho=new double [N];
	grad=new double [N];
	vxc_final=new double [N];
	rho_final=new double [N];
	
    for (int i =0;i<3;i++){
		rad[i]=new double [N];
		vconf_basis[i]=new double [N];
		veff_basis[i]=new double [N];
		}
/*****Genera malla uniforme t, y potencial*****************/
	for (int i=0;i<N;i++){
		t[i]=min+i*h;
		r[i]=exp(t[i])/Z;
		vn[i]=-Z/r[i];
		rho[i]=0.;
		vconf_new[i]=0.;
		veff_new[i]=vn[i]+vconf_new[i];
		vconf_dens[i]=0;
		veff_dens[i]=0;
		
		for (int j =0;j<3;j++){
			vconf_basis[j][i]=0;
			veff_basis[j][i]=0;
			rad[j][i]=0;
		}
	}



//****************************************************************
int n;
int l;
double n_ocup;

if(Relativistic==false){


	string orbital;

	for(int i=0;i<config.size();i++){

				n_ocup=atof(config[i].substr(2).c_str());
				orbital=config[i].substr(0,2);
				n=int(orbital[0]-'0');
				l=x[orbital[1]];
				Atom.push_back(new Orbital_norel(n,l,n_ocup,Z,-Z*Z/(2.*n*n),t,N));
				index[orbital]=i;


	}

}
else{

	string orbital;
	int j=0;
	for(int i=0;i<config.size();i++){

		        n_ocup=atof(config[i].substr(2).c_str());
				orbital=config[i].substr(0,2);
				n=int(orbital[0]-'0');
				l=x[orbital[1]];
				index[orbital]=j;
				if(l>0){
					double n_ocup_1=0;
					double n_ocup_2=0;
					switch(ocup_type){

					case Average:
						n_ocup_1=(2.*l)/(2.*l+1.)*n_ocup/2.;
					    n_ocup_2=(2.*l+2.)/(2.*l+1.)*n_ocup/2.;
					break;

					case Energy:
						for (int j=n_ocup;j>0;j--){
							if(n_ocup_1<2*l){
								n_ocup_1++;
							}
							else{
								n_ocup_2++;
							}
						}
					break;
					}

					Atom.push_back(new Orbital_rel(n,l,n_ocup_2,1,Z,-Z*Z/(2.*n*n),t,N));
					Atom.push_back(new Orbital_rel(n,l,n_ocup_1,-1,Z,-Z*Z/(2.*n*n),t,N));
				    j++;

					}

				else{Atom.push_back(new Orbital_rel(n,l,n_ocup,1,Z,-Z*Z/(2.*n*n),t,N));}
	            j++;


		}

	}



/***Genera malla uniforme t, y potencial*****************/


if(gga==false){
	vxc=new Xc_lda(N,Relativistic);
}
else{
	vxc=new Xc_gga(N);
}


};

Scf::~Scf(){
	delete [] t;
	delete [] r;
	delete [] veff_new;
//	delete [] veff_old;
	delete [] vn;
	delete [] vconf_new;
//	delete [] vconf_old;
	delete [] rho;
    
    delete [] grad;
    delete vxc;
    
    delete [] vconf_dens;
	delete [] veff_dens;
	
	delete [] vxc_final;
	delete [] rho_final;
	
    for (int i=0;i<Atom.size();i++){
    	delete Atom[i];
    }
    for (int i=0;i<3;i++){
		delete [] rad[i];
		delete [] vconf_basis[i];
		delete [] veff_basis[i];
		}
}



void Scf::run(double w,double al,double ro,double alf){
	/**************************************************************/
	alfa=alf;
	W=w;
	a=al;
	r0=ro;
	
    if(restart==true){
	    readpot();
    }
    
	for(int i=0;i<N;i++){
		if(restart==false){
	        veff_new[i]=veff_new[i]-vconf_new[i];
        }
		
		vconf_new[i]=W/(1+exp(-a*(r[i]-r0)));
		veff_new[i]=veff_new[i]+vconf_new[i];

	}

	
	
    cout<<"Wood-Saxon potential:  W: "<<W<<"  a:"<<a<<"  r0:"<<r0<<endl;

    cout<<"Functional :";
	if (gga==true){
		cout<<"PBE"<<endl;
	}else{
		cout<<"LDA"<<endl;
	}

	cout<<"Relativistic: ";
	if (Relativistic==true){
		cout<<"TRUE"<<endl;
	}
	else{
		cout<<"FALSE"<<endl;
	}


	cout<<"restart: ";
		if (restart==true){
			cout<<"TRUE"<<endl;
		}else{
			cout<<"FALSE"<<endl;
		}

	cout<<"Save: ";
	if (save==true){
		cout<<"TRUE"<<endl;
	}
	else{
		cout<<"FALSE"<<endl;
	}

	/*Se crea spline y Orbital.Se resuelve el atomo hidrogenoide veff=-Z/r */


//    #pragma omp parallel for            //TODO   este pragma es conflictivo no captura exit()
	for(int i=0;i<Atom.size();i++){
		Atom[i]->resolver(veff_new,r,W);
       
	}


	/*************************************************************/
	//Se obtiene rho inicial a partir del atomo hidrogenoide

	new_rho(rho,grad,Atom, alfa, N);
	//Se crea e inicializa los potenciales de hartree e intercambio y correlacion.y se crea veff
	Hartree vh(r,rho,t,h,Z,N);

    vxc->update(r,rho,grad,N);
	for(int i=0;i<N;i++){
	        veff_new[i]=vn[i]+vh[i]+vxc->operator[](i)+vconf_new[i];
	     }


#pragma omp parallel for
	for(int i=0;i<Atom.size();i++){
		Atom[i]->resolver(veff_new,r,W);
        
	}

	double *e1;
	double *e2;
	e1=new double [Atom.size()];
	e2=new double [Atom.size()];



	for(int i=0;i<Atom.size();i++){
		e1[i]=0.;
		e2[i]=Atom[i]->energy();

	}

	double error=0.00000001;
	int iteraciones=0;
	while(fabs(e2[Atom.size()-1]-e1[Atom.size()-1])> error ){
	    new_rho(rho,grad,Atom, alfa, N);
		vh.update(r,rho,t,h);
		vxc->update(r,rho,grad,N);
	    for(int i=0;i<N;i++){
	        veff_new[i]=vn[i]+vh[i]+vxc->operator[](i)+vconf_new[i];
	     }
	     e1[Atom.size()-1]=e2[Atom.size()-1];

#pragma omp parallel for
	     for(int i=0;i<Atom.size();i++){
	    	 Atom[i]->resolver(veff_new,r,W);
	    	 e2[i]=Atom[i]->energy();

	     }
	     iteraciones++;
	     if(fabs(e2[Atom.size()-1]-e1[Atom.size()-1])<0.00001){
	    	 alfa=0.5;
	     }
	     if (iteraciones>100)break;
	}
	cout<<"iteraciones: "<<iteraciones<<endl;
	cout<<"Eh"<<"   "<<vh.energy()<<endl;
	cout<<"Exc"<<"  "<<vxc->energy()<<endl;

	for (int i=0;i<Atom.size();i++){
		cout<<Atom[i]->prin()<<angular[Atom[i]->ang()]<<"  "<<Atom[i]->energy()<<endl;

	}
	    cout<<endl;
	/******Se libera spline y acc*********************/
   
    if(save==true){
    	writepot();
    }

	delete [] e1;
	delete [] e2;

}
void Scf::energy(vector<double> &e,vector<double> &ocupation){
    int l;
	if(Relativistic==false){
		for(int i=0;i<3;i++){
			if (index.count(SK[i])>0){                          //Comprueba si el orbitalseleccionado para las tabla es un orbital calculado.
				e[i]=Atom[index[SK[i]]]->energy();
				ocupation[i]=Atom[index[SK[i]]]->ocup();
				//U[i]=Hubbard(scf,Atom,index[SK[i]]);          // Desmarcar para habilitar Hubbard

			}
		}
	}
	else{
		for(int i=0;i<3;i++){
			if (index.count(SK[i])>0){                           //Comprueba si el orbital seleccionado para las tabla es un orbital calculado.
				l=x[SK[i][1]];
				if(l>0){
				    e[i]=(l/(2.*l+1.))*Atom[index[SK[i]]+1]->energy()+((l+1.)/(2.*l+1.))*Atom[index[SK[i]]]->energy();
				    ocupation[i]=Atom[index[SK[i]]]->ocup()+Atom[index[SK[i]]+1]->ocup();
				    //U[i]=Hubbard(scf,Atom,index[SK[i]]);          // Desmarcar para habilitar Hubbard


				}
			    else{
			    	e[i]=Atom[index[SK[i]]]->energy();
			    	ocupation[i]=Atom[index[SK[i]]]->ocup();


			    }

			}
		}
	}
}

void Scf::energy(int i){
    int l;
	if(Relativistic==false){
		
			if (index.count(SK[i])>0){                          //Comprueba si el orbitalseleccionado para las tabla es un orbital calculado.
				econf[i]=Atom[index[SK[i]]]->energy();
				
				//U[i]=Hubbard(scf,Atom,index[SK[i]]);          // Desmarcar para habilitar Hubbard

			}
		
	}
	else{
		
			if (index.count(SK[i])>0){                           //Comprueba si el orbital seleccionado para las tabla es un orbital calculado.
				l=x[SK[i][1]];
				if(l>0){
				    econf[i]=(l/(2.*l+1.))*Atom[index[SK[i]]+1]->energy()+((l+1.)/(2.*l+1.))*Atom[index[SK[i]]]->energy();
				    
				    //U[i]=Hubbard(scf,Atom,index[SK[i]]);          // Desmarcar para habilitar Hubbard


				}
			    else{
			    	econf[i]=Atom[index[SK[i]]]->energy();
			    	

			    }

			}
		
	}
}



void Scf::save_basis(){
	int l;
	vector<double> e={0,0,0};
	vector<double> nocup={0,0,0};
	energy(econf,nocup);
	if (Relativistic==true){

		for(int i=0;i<3;i++){
			if(index.count(SK[i])>0){
				l=x[SK[i][1]];
				if(l>0){
					for(int j=0;j<N;j++){
						rad[l][j]=(*(Atom[index[SK[i]]+1]))(j)*l/(2.*l+1.)+(*(Atom[index[SK[i]]]))(j)*(l+1.)/(2.*l+1.);
					}
				}
				else{
					for(int j=0;j<N;j++){
						rad[l][j]=(*(Atom[index[SK[i]]]))(j);
					}
				}

				
			}
		}
	}
	else{
		for(int i=0;i<3;i++){
			if(index.count(SK[i])>0){
				l=x[SK[i][1]];
				for(int j=0;j<N;j++){
						rad[l][j]=(*(Atom[index[SK[i]]]))(j);
			    }
			}
		}
	}
}

void Scf::save_basis(int i){
	int l;
	vector<double> e={0,0,0};
	vector<double> nocup={0,0,0};
	energy(i);
	
	if (Relativistic==true){

		
			if(index.count(SK[i])>0){
				l=x[SK[i][1]];
				if(l>0){
					for(int j=0;j<N;j++){
						rad[l][j]=(*(Atom[index[SK[i]]+1]))(j)*l/(2.*l+1.)+(*(Atom[index[SK[i]]]))(j)*(l+1.)/(2.*l+1.);
					}
				}
				else{
					for(int j=0;j<N;j++){
						rad[l][j]=(*(Atom[index[SK[i]]]))(j);
					}
				}

				
			}
			}
	else{
		if(index.count(SK[i])>0){
			l=x[SK[i][1]];
			for(int j=0;j<N;j++){
				rad[l][j]=(*(Atom[index[SK[i]]]))(j);
			    }
			}
		
	}
}


void Scf::readpot(){
	ifstream guess(restart_file);
	IStreamWrapper isw(guess);
	Document input;
	input.ParseStream(isw);

	Value en=input["orbital energies"].GetArray();
	for (int i=0;i<Atom.size();i++){
		    Atom[i]->set_e(en[i].GetDouble());
		}

	Value veff_in=input["Veff"].GetArray();
	Value rho_in=input["rho"].GetArray();
	for(int i=0;i<N;i++){
		veff_new[i]=veff_in[i].GetDouble();
		rho[i]=rho_in[i].GetDouble();

	}

    guess.close();
}

void Scf::writepot(){
        ofstream pot_file(prefix+".pot");
	for (int i=0;i<Atom.size();i++){
			pot_file<<Atom[i]->energy()<<endl;
		}

	for(int i=0;i<N;i++){
			pot_file<<veff_new[i]<<" "<<rho[i]<<endl;
	}

	pot_file.close();

}

void Scf::write_atom(){
	Document out;
    out.SetObject();
    
    Value conf_pot(kObjectType);
    Document::AllocatorType& allocator = out.GetAllocator();
	out.AddMember("Z",Z,allocator);

	conf_pot.AddMember("W",W,allocator);
	conf_pot.AddMember("a",a,allocator);
	conf_pot.AddMember("r0",r0,allocator);
	out.AddMember("Confinament potential",conf_pot,allocator);
    
	Value orbital_energy(kArrayType);
    for (int i = 0; i < Atom.size(); i++){
        orbital_energy.PushBack(Atom[i]->energy(), allocator);
	}
	out.AddMember("orbital energies",orbital_energy,allocator);

	Value t_arr(kArrayType);
    for (int i = 0; i < N; i++){
        t_arr.PushBack(t[i], allocator); // Grilla t
    }
	out.AddMember("t",t_arr,allocator);

	Value Veff_arr(kArrayType);
    for (int i = 0; i < N; i++){
        Veff_arr.PushBack(veff_dens[i], allocator);
    }
	out.AddMember("Veff",Veff_arr,allocator);

	Value Vxc_array(kArrayType);
    for (int i = 0; i < N; i++){
        Vxc_array.PushBack(vxc_final[i], allocator);
    }
	out.AddMember("Vxc",Vxc_array,allocator);

	Value rho_arr(kArrayType);
    for (int i = 0; i < N; i++){
        rho_arr.PushBack(rho[i], allocator);
    }
	out.AddMember("rho",rho_arr,allocator);


	Value psi_s(kArrayType);
	for (int i = 0; i < N; i++){
        psi_s.PushBack(rad[0][i], allocator);
    }
	out.AddMember("s",psi_s,allocator);

	Value psi_p(kArrayType);
	for (int i = 0; i < N; i++){
        psi_p.PushBack(rad[1][i], allocator);
    }
	out.AddMember("p",psi_p,allocator);

	Value psi_d(kArrayType);
	for (int i = 0; i < N; i++){
        psi_d.PushBack(rad[2][i], allocator);
    }
	out.AddMember("d",psi_d,allocator);

	out.AddMember("es",econf[0],allocator);
	out.AddMember("ep",econf[1],allocator);
	out.AddMember("ed",econf[2],allocator);
        
    std::ofstream ofs(prefix+"_out.json");
    OStreamWrapper osw(ofs);
 
    PrettyWriter<OStreamWrapper> writer(osw);
	
    out.Accept(writer); 

	ofs.close();

}



void Scf::save_dens(){
	for (int j=0;j<N;j++){
		veff_dens[j]=veff_new[j]-vconf_new[j];
		vconf_dens[j]=vconf_new[j];
		vxc_final[j]=vxc->operator[](j);
		rho_final[j]=rho[j];
		}
	
	
	}

void Scf::save_pot(int i){
	for (int j=0;j<N;j++){
		veff_basis[i][j]=veff_new[j]-vconf_new[j];
		vconf_basis[i][j]=vconf_new[j];
		}
	
	
	}
void Scf::save_pot(){
	for (int i=0;i<3;i++){
		for (int j=0;j<N;j++){
			veff_basis[i][j]=veff_new[j]-vconf_new[j];
			vconf_basis[i][j]=vconf_new[j];
			}
		}
	
	
	}

double* Scf::radial(int l){
	return rad[l];
	}
         

