/*
 * SKtable.cpp
 *
 *  Created on: Feb 27, 2018
 *      Author: leandro
 */

#include "SKtable.h"
#include <iomanip>
typedef std::numeric_limits< double > dbl;

SKtable::SKtable(Scf &A,Scf &B) {
	// TODO Auto-generated constructor stub
	step=0.02;
	rmin=0.4;
	rmax=12;
	ngrid=50;
	
	homonuclear=true;
	double *ta=A.tgrid();
	double *tb=B.tgrid();;
	int Na=A.Nt();
	int Nb=B.Nt();
	Z1=A.z();
	Z2=B.z();
	r1min=exp(ta[0])/Z1;
	r2min=exp(tb[0])/Z2;
	r1max=exp(ta[Na-1])/Z1;
	r2max=exp(tb[Nb-1])/Z2;
	
	for (int i=0;i<3;i++){
		A_spline.push_back(new MySpline(ta,A.radial(i),Na));
	    
		B_spline.push_back(new MySpline(tb,B.radial(i),Nb));
		
		
		}
		
	for (int i=0;i<3;i++){
	    //Veffa.push_back(new MySpline(ta,A.Veff_basis(i),Na));
	    //Vconf=new MySpline(tb,B.Vconf_new(),Nb);  //ver si no es alreves
	    Veffb.push_back(new MySpline(tb,B.Veff_basis(i),Nb));
	    Vconf.push_back(new MySpline(tb,B.Vconf_basis(i),Nb));
    }
    Veffa=new MySpline(ta,A.Veff_dens(),Na);
	Veff0b=new MySpline(tb,B.Veff_dens(),Nb);
	Vxca=new MySpline(ta,A.get_vxc_final(),Na);
	Vxcb=new MySpline(tb,B.get_vxc_final(),Nb);
	dens_a=new MySpline(ta,A.get_rho_final(),Na);
	dens_b=new MySpline(tb,B.get_rho_final(),Nb);
		
	eb.push_back(B.get_econf(0));
	eb.push_back(B.get_econf(1));
	eb.push_back(B.get_econf(2));
	eb.push_back(B.get_econf(1));
	eb.push_back(B.get_econf(1));
	eb.push_back(B.get_econf(2));
	eb.push_back(B.get_econf(2));
	eb.push_back(B.get_econf(2));
	eb.push_back(B.get_econf(2));
	eb.push_back(B.get_econf(2));
	superposition=potential;
	
	
	for (int i=0;i<3;i++){
	    H_onsite[i]= onsite(B.Veff_basis(i),B.Vconf_basis(i),B.Veff_dens(),B.radial(i),B.get_econf(i),Nb,tb);
	
	}
	calc_onsite=false;
	
	
	
}

SKtable::~SKtable() {
	// TODO Auto-generated destructor stub


    
	//delete Vconf;
	
	delete	Veffa;
	delete Veff0b; 
    delete Vxca;
	delete Vxcb;
	delete dens_a;
	delete dens_b;
	
    for (int i=0;i<3;i++){
		delete A_spline[i];
	    delete B_spline[i];
	    //delete	Veffa[i];
		delete Veffb[i];
		delete Vconf[i];	
		
		}

}

void SKtable::create( bool type){
    
    homonuclear=type;
    
 


};

void SKtable::run( vector<double> &e,vector<double> &U,vector<double> &ocupation,string archivo){
	double d=rmin/2;

	gauss g(ngrid);

	ofstream salida2(archivo.c_str());

	salida2<<step<<" "<<int((rmax-step)/step)<<endl;
    
	if(homonuclear==true){
		if (calc_onsite==true){
		    salida2<<H_onsite[2]<<" "<<H_onsite[1]<<"  "<<H_onsite[0]<<" 0.0  "<<U[2]<<"  "<<U[1]<<"  "<<U[0]<<"  "<<ocupation[2]<<"  "<<ocupation[1]<<"  "<<ocupation[0]<<endl;
	    }
	    else{
	        salida2<<e[2]<<" "<<e[1]<<"  "<<e[0]<<" 0.0  "<<U[2]<<"  "<<U[1]<<"  "<<U[0]<<"  "<<ocupation[2]<<"  "<<ocupation[1]<<"  "<<ocupation[0]<<endl;
	    }
	}
	salida2<<"12.01, 19*0.0"<<endl;

	int j=0;

	while(step*(1+j)<rmin){
		salida2<<"20*0.0,"<<endl;
	    j++;
	}
    int ndist=round((rmax-rmin)/step+0.5);
//	double overlap[ndist][10];
	double H[ndist][10];
	double s[ndist][10];
	double v[ndist][10];
	

#pragma omp parallel for collapse(2)
for(int j=0;j<ndist;j++){
	for(int i=0;i<10;i++){
			H[j][i]=0.00;
			//overlap[j][i]=0.00;
			s[j][i]=0.0;
			v[j][i]=0.0;
			}
	}		
	
	

	
#pragma omp parallel for
	for (int k=0;k<ndist;k++){
		d=rmin+step*k;
		
	    if (superposition==potential){
		    g.integrate_potential(Veffa,Veffb, Vconf,Veff0b,A_spline,B_spline,s[k],v[k],Z1,Z2,r1min, r2min, r1max, r2max,d/2);
	        }
	    else if(superposition==density){
			g.integrate_density(Veffa,Veffb, Vconf,Veff0b,A_spline,B_spline,Vxca,Vxcb,dens_a,dens_b,s[k],v[k],Z1,Z2,r1min, r2min, r1max, r2max,d/2);
			
			}
	    
	    for(int i=0;i<10;i++){
	    	
	    		//overlap[k][i]=s[k][i];
	    		H[k][i]=eb[i]*s[k][i]+v[k][i];
	    	
		}




	}
salida2.precision(dbl::max_digits10);	    
for (int k=0;k<ndist;k++){
		for(int i=9;i>=0;i--){
			salida2<<H[k][i]<<"  ";
		}

		for(int i=9;i>=0;i--){
				//salida2<<overlap[k][i]<<"  ";
				salida2<<s[k][i]<<"  ";
		}
		salida2<<endl;
	}
	    


	



	salida2.close();

};
double  SKtable::onsite(double *Veffb, double *Vconf,double *Veff0b,double *R,double ebconf,int N,double *t){
	
	 double h=t[1]-t[0];
	 double f[N];
	 for (int j=0;j<N;j++){  
	      
	      f[j]=(Veff0b[j]-Veffb[j]-Vconf[j])*R[j]*R[j]*exp(t[j])/Z2;
		
	}
	
	return ebconf+simpson(f,h,N); 
	}
	
	
	
void SKtable::set_grid(int a){ngrid=a;}
void SKtable::set_rmax(double a){rmax=a;}
void SKtable::set_rmin(double c){rmin=c;}
void SKtable::set_step(double z){step=z;}
void SKtable::set_superposition(string sup){
	if (sup=="potential"){
	    superposition=potential;
	    }
	else if (sup=="density"){
		superposition=density;
		}
	else{
		cout<<sup<<"is not a valid superposition type,posible value are potential and density,using potential as default"<<endl;
		
		}	
	}
