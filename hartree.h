/*
 * hartree.h
 *
 *  Created on: Mar 9, 2017
 *      Author: leandro
 */

#ifndef HARTREE_H_
#define HARTREE_H_



#include "Poisson.h"
#include "globals.h"


/** Clase para el calculo de energia y potencial de Hartree.
 * 
 */
class Hartree{
    private:
        double *v;
        double Eh;
        int N;
        int Z;
    public:
        /**Contructor.
         * @param r : grilla distacia.
         * @param rho: grilla densidad
         * @param t: grilla en escala ln.
         * @param Z: numero atomico.
         * @param N: tamaño grilla.
         */
        Hartree(double *r,double *rho,double *t,double h,int z,int n);
        ~Hartree();
        double energy(){return Eh;};

        /** Actualiza el potencial de Hartree.
         * @param r : grilla distacia.
         * @param rho: grilla densidad
         * @param t: grilla en escala ln.
         * @param Z: numero atomico.
         * @param N: tamaño grilla. 
         */
        void update(double *r,double *rho,double *t,double h);
        double operator[](int);
    };

#endif /* HARTREE_H_ */
