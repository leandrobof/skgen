#include "globals.h"
#include <vector>
#include "SKtable.h"

void read_table_opt(char* archivo,SKtable& sk);

struct atom{
	vector<vector<double>> param_dens;
	vector<vector<double>> param_basis;
	vector<vector<double>> param_s;
	vector<vector<double>> param_p;
	vector<vector<double>> param_d;
	vector<string> symbol;
	vector<vector<double>> e;
	vector<vector<double>> U;
	vector<bool> has_param_dens;
	vector<vector<bool>> has_param_orbital; 
	};
atom read_table_atom(char* archivo);