/*
 * Orbital.h
 *
 *  Created on: Nov 14, 2016
 *      Author: leandro
 */
#include "globals.h"
#include "Ode.h"


#ifndef ORBITAL_H_
#define ORBITAL_H_

double simpson(double *f,double h,int N);


typedef struct
{
  double * c;
  double * g;
  double * diag;
  double * offdiag;
} cspline_state_t;

struct spline_coefs{double b,c,d;} ;

static inline void coeff_calc (const double c_array[], double dy, double dx, size_t index,spline_coefs *co);


void coeff_extract(cspline_state_t *coefs,double *y,double *x,spline_coefs *co ,int n);
		

/** Clase para la creacion y manejo de splines.
 * Sea una funcion y=f(x).
 * La grilla x debe estar divida de forma equidistante.
*/
class MySpline{
	private:
	        int n;
	        double *x;
	        double *y;
	        spline_coefs *sp_coefs;
	        double h;
	public:
			/**Constructor.
			 * @param  x_array : grilla absisa.
			 * @param  y_array : grilla ordenada. 
			 * @param  npuntos : Tamaño de la grilla
			 */
	       MySpline(double *x_array,double *y_array,int npuntos){
			  x=x_array;
			  y=y_array; 
			  n=npuntos;
			  h=x_array[1]-x_array[0];
			  sp_coefs=new spline_coefs[npuntos];
			  
			  
			  gsl_interp_accel *acc = gsl_interp_accel_alloc();
                          gsl_spline *spline_cubic = gsl_spline_alloc(gsl_interp_cspline, n);
                          gsl_spline_init(spline_cubic, x_array, y_array, n);
                          
			  cspline_state_t *coefs=(cspline_state_t *)(spline_cubic-> interp -> state);
			  coeff_extract(coefs,y_array,x_array,sp_coefs,n); 
			  
			  gsl_spline_free(spline_cubic);
			  gsl_interp_accel_free(acc);
			   };
	       ~MySpline(){
			   
			   delete [] sp_coefs;
			   
			   };

			 /**Interpola.
			  * @param xval : valor a interpolar.
			  */
			    
			inline double inter(double xval){
				if (xval<x[0] or xval>x[n-2]){
					return 0;
					}
				else{
				    int index=int((xval-x[0])/h);
	        	    double y0=y[index];
	                double delx = xval - x[index];
	                return  y0 + delx * (sp_coefs[index].b + delx * (sp_coefs[index].c + delx * sp_coefs[index].d));
			    }
				
				};   
	       
	       
};




/**Clase para el calculo de orbitales atomicos.
 * Se resuelve la ecuacion de Schrodinger para un orbital atomico.
 * Shooting method, se resuelven dos ODE , una de afuera hacia adentro y otra de adentro hacia afuera. Si el valor de prueba e coincide con el autvalor del orbital,
 * entoces las dos curvas empalman perfectamente. Se itera hasta encontrar el valor de e correcto. 
 */
class Orbital {
	public:
		Orbital(int principal,int angular,double num_ocupacion,int z,double energy,double *t,int N);
		Orbital(const Orbital &orb);
		virtual ~Orbital();
		virtual void outward(double *,double *)=0;
		virtual void inward(double*,double *,double W)=0;
		virtual double correct_e(double *r)=0;
		virtual void final(double r,double *R,double W)=0;
		virtual void inicial(double r,double *R)=0;
		virtual void dens(double *r)= 0;
		void estimate_a();
		double energy(){return e;};
		int prin(){return n;};
		int ang(){return l;};
		void set_ocup(double o){noc=o;}
		void set_e(double en){e=en;}
		double ocup(){return noc;};
		void resolver(double *,double *,double W);
		virtual void print()=0;
		virtual void radial()=0;
		double operator[](int);
		double operator()(int);
		double grd(int i){return grad[i];};


	protected:
		int n;
		int l;
		double e;
		int Z;
		int Nt;
		int nodos;
		double h;
		double *t;
		double tinf;
		double t0;
		double tk;
		double noc;
		double co;
		double ci;
		double a;
		double dRi;
		double dRo;
		double *Rl;
		double *rho;
		double *grad;
		int max;
};

/**Orbitales no relativistas
*/
class Orbital_norel :public Orbital{

	public:
		Orbital_norel(int principal,int angular,double num_ocupacion,int z,double energy,double *t,int N);
		virtual ~Orbital_norel();
		virtual void dens(double *r);
		virtual void radial();
		virtual void final(double r,double *R,double W);
		virtual void inicial(double r,double *R);
		virtual double correct_e(double *r);
		virtual void outward(double *,double *);
		virtual void inward(double*,double *,double W);
		virtual void print();
	};

/**Orbitales relativistas.
 * 
 */ 
class Orbital_rel : public Orbital{
	private:
		int k;
		double *Ql;
	public:
		Orbital_rel(int principal,int angular,double num_ocupacion,int s,int z,double energy,double *t,int N);
		Orbital_rel(const Orbital_rel &orb);
		Orbital_rel operator=( const Orbital_rel& orb );
		~Orbital_rel();
		virtual void dens(double *r);
		virtual void radial();
		virtual void final(double r,double *R,double W);
		virtual void inicial(double r,double *R);
		virtual double correct_e(double *r);
		virtual void outward(double *,double *);
		virtual void inward(double*,double *,double W);
		virtual void print();
	};



#endif /* ORBITAL_H_ */
