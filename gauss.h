/*
 * gauss.h
 *
 *  Created on: Apr 5, 2017
 *      Author: leandro
 */

#ifndef GAUSS_H_
#define GAUSS_H_
#include "Integrand.h"
#include <gsl/gsl_integration.h>
#include <xc.h>

using namespace std;
class gauss {
    private:
        int n;
        double c2;
        gsl_integration_glfixed_table *t;
        double *x1,*x2,*w1,*w2,*atanh_x,*cos_2x,*sin_v,*cos_v;
        xc_func_type correlacion;
        xc_func_type exchange;
    public:    
        gauss();
        gauss(int N);
        virtual ~gauss();
        void integrate_potential(MySpline *veffa,vector<MySpline*> &veffb,vector<MySpline*> &vconfb,MySpline *veff0b,vector<MySpline*> &A,vector<MySpline*> &B,double*s,double*y,int z1,int z2,double r1min,double r2min,double rmax1,double rmax2,double distance);
        void integrate_density(MySpline *veffa,vector<MySpline*> &veffb,vector<MySpline*> &vconfb,MySpline *veff0b,vector<MySpline*> &A,vector<MySpline*> &B,MySpline *vxca,MySpline *vxcb,MySpline *dens_a,MySpline *dens_b,double*s,double*y,int z1,int z2,double r1min,double r2min,double rmax1,double rmax2,double distance);
        void generate_grid(double a,double c1,double *rr,double *t1,double *t2,double *sin_1,double *sin_2,double *cos_1,double *cos_2);
    };

#endif /* GAUSS_H_ */
