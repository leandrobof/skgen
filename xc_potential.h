/*
 * xc_potential.h
 *
 *  Created on: Mar 27, 2017
 *      Author: leandro
 */

#ifndef XC_POTENTIAL_H_
#define XC_POTENTIAL_H_
#include <xc.h>
#include "globals.h"

/**Clase para trabajar con intercambio y correlacion.
 * Calcula potencial (vxc) y la energia (Exc) de intercambio y correlacion a partir de densidad y gradiente.
 */ 
class Xc{
	protected:
		double *ec;
		double *ex;
		double *vx;
		double *vc;
		double *vxc;

		double Exc;
		xc_func_type correlacion;
		xc_func_type exchange;
	public:
	 	/**Constructor.
		 *@param N : tamaño de las grillas de las cantidad con las que luego se haran el calculo (r,rho,grad)
		*/  
		Xc(int N);

		virtual ~Xc();

		/**Actualiza los valores de vxc  y Exc dadas la densidad y el gradiente y la distancia radial.
		 * @param r: grilla radial.
		 * @param rho : grilla densidad.
		 * @param grad : grilla gradiente.
		 * @param N: tamaño de las grillas.
		 */
		virtual void update(double *r,double *rho,double *grad,int N)=0;

		/**Devuelve el valor de potencial intercambio vxc[i].*/
		double operator[](int i);

		/**Devuelve energia intercambio correlacion*/
		double energy(){return Exc;};
	};

/**Correlacion intercambio: LDA.*/
class Xc_lda :public Xc{
public:
	Xc_lda(int N,bool);
	virtual ~Xc_lda();
	virtual void update(double *r,double *rho,double *grad,int N);

};

/**Correlacion intercambio: GGA (PBE) */
class Xc_gga :public Xc{
private:
	double *vcsigma;
    double *vxsigma;
	double *sigma;
public:
	Xc_gga(int N);
	virtual ~Xc_gga();
	virtual void update(double *r,double *rho,double *grad,int N);

};

#endif /* XC_POTENTIAL_H_ */
