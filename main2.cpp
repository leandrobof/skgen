/*
 * main.cpp
 *
 *  Created on: Nov 15, 2016
 *      Author: leandro
 */

#include "Orbital.h"
#include "gauss.h"
#include "SKtable.h"
#include "scf.h"
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <chrono>
#include "read_param.h"

using namespace std;



//double Hubbard(Scf &a,vector<Orbital*> &b,const int &c );



int main(int argc,char *argv[]){	


vector<string> simbolo;
vector<vector<double>> e;
vector<vector<double>> ocupation;
vector<vector<double>> U;
vector<Scf*> Atoms;









//***Lee  Atomo el archivo, y los parametros de confinamiento
atom natom;
try{
	natom=read_table_atom(argv[1]);
	simbolo=natom.symbol;
}catch(string &msg2){
	cerr<<msg2<<"\n";
	return 1;
}


//********************************************
auto start= std::chrono::high_resolution_clock::now();
for(int i=0;i<simbolo.size();i++){
	e.push_back({0,0,0});
	ocupation.push_back({0,0,0});
	U.push_back({0.,0.,0.});
	Atoms.push_back(new Scf);
}

try{
//#pragma omp parallel for
for(int i=0;i<simbolo.size();i++){	
// Inicializa y corre atomo sin confinamiento.
	Atoms[i]->initialize(simbolo[i]);		
    Atoms[i]->run(0,1,1,0.2);
    Atoms[i]->energy(e[i],ocupation[i]);
   
// Corre confinamientos bases y obtiene bases y potenciales.    
    Atoms[i]->run(natom.param_basis[i][0],natom.param_basis[i][1],natom.param_basis[i][2],0.2);
    Atoms[i]->save_basis();
	Atoms[i]->save_pot();
	Atoms[i]->save_dens();
	Atoms[i]->write_atom();
	
// Corre  confinamiento densidad y obtienen pot.
    if (natom.has_param_dens[i]==true){
		Atoms[i]->run(natom.param_dens[i][0],natom.param_dens[i][1],natom.param_dens[i][2],0.2);
		Atoms[i]->save_dens();
    }
// Corre confinamientos bases y obtiene bases y potenciales.
    if (natom.has_param_orbital[i][0]==true){
		Atoms[i]->run(natom.param_s[i][0],natom.param_s[i][1],natom.param_s[i][2],0.2);
		Atoms[i]->save_basis(0);
		Atoms[i]->save_pot(0);
		}
	if (natom.has_param_orbital[i][1]==true){
		Atoms[i]->run(natom.param_p[i][0],natom.param_p[i][1],natom.param_p[i][2],0.2);
		Atoms[i]->save_basis(1);
		Atoms[i]->save_pot(1);		
		}		
	if (natom.has_param_orbital[i][2]==true){
		Atoms[i]->run(natom.param_d[i][0],natom.param_d[i][1],natom.param_d[i][2],0.2);
		Atoms[i]->save_basis(2);
		Atoms[i]->save_pot(2);
		}		

//  Atoms[i]->save_basis(0);
//  Atoms[i]->save_basis(1);
//	Atoms[i]->save_basis(2);	

	
//Remplaza valores e[] y U[] si es necesario
	for (int j=0;j<3;j++){
		if (natom.U[i][j] >= 0){
			U[i][j]=natom.U[i][j];
		}
		if (natom.e[i][j] <= 2.){
			e[i][j]=natom.e[i][j];
		}
					
	}
    
}
}catch(string &msg){
	cerr<<msg<<"\n";
	return 1;
}	

auto start_table = std::chrono::high_resolution_clock::now();
std::chrono::duration<double> elapsed1 = start_table- start;
cout<<"Time Atomos: "<<elapsed1.count()<<endl;


cout<<"Calculating skf files"<<endl;
try{
for (int i=0;i<Atoms.size();i++){
	for (int j=0;j<Atoms.size();j++){
		SKtable sk(*(Atoms[i]),*(Atoms[j]));
		read_table_opt(argv[1],sk);
        
		if (i !=j){
			sk.create(false);
		}
		else {
			sk.create(true);
		};
        
		sk.run(e[i],U[i],ocupation[i],simbolo[i]+"-"+simbolo[j]+".skf");

		cout<<simbolo[i]+"-"+simbolo[j]+".skf"<<endl;
	}
}
}catch(string &msg2){
	cerr<<msg2<<"\n";
	return 1;
}	
auto end = std::chrono::high_resolution_clock::now();
std::chrono::duration<double> elapsed = end - start_table;

cout<<"Time tablas: "<<elapsed.count()<<endl;



for (int i=0;i<Atoms.size();i++){
	delete Atoms[i];



}



//*******************
return 0;
}





	

/*
double Hubbard(Scf &a,vector<Orbital*> &b,const int &c ){
	double h=0.001;
	double n=b[c]->ocup();
	double nf=n+0.5*h;
	double nb=n-0.5*h;
	double e=b[c]->energy();
	double ef;
	double eb;
	b[c]->set_ocup(nf);
	a.run(b,0,1,1,0.3);
	ef=b[c]->energy();
	b[c]->set_ocup(nb);
	a.run(b,0,1,1,0.3);
	eb=b[c]->energy();
	b[c]->set_ocup(n);
	a.run(b,0,1,1,0.3);
	return (ef-eb)/h;
}
*/

