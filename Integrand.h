/*
 * Integrand.h
 *
 *  Created on: Apr 5, 2017
 *      Author: leandro
 */

#ifndef INTEGRAND_H_
#define INTEGRAND_H_
#include <gsl/gsl_spline.h>
#include <math.h>
#include <iostream>
#include "Orbital.h"

using namespace std;

	inline double ss(double sin1,double sin2,double cos1,double cos2){return 0.5;}

	inline  double sp(double sin1,double sin2,double cos1,double cos2){return sqrt(3)/2.*cos2;}

	inline  double pp_sig(double sin1,double sin2,double cos1,double cos2){return (3/2.)*cos1*cos2;}

	inline  double pp_pi(double sin1,double sin2,double cos1,double cos2){return (3/4.)*sin1*sin2;}

	inline double sd(double sin1,double sin2,double cos1,double cos2){return sqrt(5)/4.*(3*cos2*cos2-1);}

	inline  double pd_pi(double sin1,double sin2,double cos1,double cos2){return (1*sqrt(45)/4.)*sin1*sin2*cos2;}

	inline  double pd_sig(double sin1,double sin2,double cos1,double cos2){return 1*sqrt(15)/4.*cos1*(3*cos2*cos2-1);}

	inline  double dd_sig(double sin1,double sin2,double cos1,double cos2){return 5/8.*(3*cos2*cos2-1)*(3*cos1*cos1-1);}

	inline  double dd_pi(double sin1,double sin2,double cos1,double cos2){return (15/4.)*sin1*cos1*sin2*cos2;}

	inline  double dd_del(double sin1,double sin2,double cos1,double cos2){return (15/16.)*sin1*sin1*sin2*sin2;}


#endif /* INTEGRAND_H_*/
